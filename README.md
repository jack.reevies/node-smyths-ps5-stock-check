## Requirements

Node v12 (nothing newer)

## What Do?

Polls the symths toy store every minute to check if PS5 is in stock

## How to Use?

+ Go to ```https://www.smythstoys.com/``` in your web browser with dev tools open
+ Click on any request and find the `Cookie` header sent off
+ Extract the `JSESSIONID=*********************;` part and paste it in `settings.json`

Run with  ```node --http-parser=legacy index.js```

+ `-http-parser=legacy` is necessary because smyths use Incapsula for a CDN and they throw out malformed headers to prevent bots
  - This option wa sremoved in later versions of node. Follow the madness here: https://github.com/nodejs/node/issues/27711

+ You might actually be able to get away without changing the JSESSIONID, but if things start to fail, replace it