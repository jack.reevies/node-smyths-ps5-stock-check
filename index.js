const dongers = require('@spacepumpkin/dongers')
const fetch = require('node-fetch')
const pino = require('pino')
const log = pino({ level: process.env.LOG_LEVEL || 'debug', prettyPrint: { translateTime: true } })
const settings = require('./settings.json')

// Find a JSessionId cookie in any request the browser made to any page on smyths
const jSessionCookieString = settings.jSessionIdString
const agent = settings.agent

async function start() {
  try {
    const { csrf, newCookieString } = await getCSRFToken()
    const stock = await getStock(csrf, newCookieString)
    isInStock(stock)
  } catch (e) {
    log.error(e)
  }
}

function isInStock(stock) {
  const inStock = stock.data.filter(o => o.stockLevelStatusCode !== 'outOfStock' || o.stockLevel !== '0')
  if (inStock.length) {
    log.info(`In stock at: ${inStock.map(o => `In stock at ${o.displayName} (${o.stockLevel} in stock)`).join('\r\n')}`)
  } else {
    log.info(`None in stock ${dongers.getRandom(dongers.angry)}`)
  }
}

async function getStock(csrf, newCookieString) {
  const res = await fetch('https://www.smythstoys.com/uk/en-gb/store-pickup/191259/pointOfServices', {
    method: 'POST',
    headers: {
      "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
      "Cookie": newCookieString,
      "User-Agent": agent,
      Accept: '*/*',
      'Accept-Encoding': 'gzip, deflate, br'
    },
    body: `cartPage=false&entryNumber=0&latitude=&longitude=&searchThroughGeoPointFirst=false&xaaLandingStores=false&CSRFToken=${csrf}`,
    redirect: 'manual'
  })
  if (res.status !== 200) {
    throw new Error(`Failed getting pointOfServices (response was not 200 OK)`)
  }
  try {
    const json = await res.json()
    return json
  } catch (e) {
    throw new Error(`Data back from pointOfServices was not valid JSON`)
  }
}

async function getCSRFToken() {
  const res = await fetch('https://www.smythstoys.com/uk/en-gb/video-games-and-tablets/playstation-5/playstation-5-consoles/playstation-5-console/p/191259', {
    headers: {
      "Cookie": jSessionCookieString
    }
  })
  const text = await res.text()
  const headers = res.headers.raw()
  const newCookies = headers['set-cookie'].reduce((acc, obj) => {
    if (obj.startsWith('incap') || obj.startsWith('visid') || obj.startsWith('JSESSIONID')) {
      return `${acc}${obj.substring(0, obj.indexOf(";"))}; `
    }
    return acc
  }, '')
  const regex = /<input type="hidden" name="CSRFToken" value="(.+?)" \/>/gm.exec(text)
  if (!regex) {
    throw new Error(`Failed to get CSRF token`)
  }
  return { csrf: regex[1], newCookieString: newCookies }
}

start()
setInterval(start, settings.interval)
